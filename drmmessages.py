# -*- coding: utf-8 -*-
"""
Lee 3 numeros determina si con los 2 anteriores en cualquier 
orden se puede sacar el tercero
"""
import sys


SPACE = ' '
NOTHING = ''
 
def leer_entero():
    return int(leer_sinsalto())
    
def leer_enteros():
    return list(map(int,leer_lista()))
    
def leer_reales():
    return map(float,leer_lista())
    
def leer_lista():
    return leer_sinsalto().split(" ")
    
def leer_sinsalto():
    return leer_linea().strip()    
    
def leer_linea():
    return sys.stdin.readline()
    
def rotation_value(m):
    r = 0
    for c in m:
        r += LETTERS.index(c)
    return r
    
def rotar_string(string, valor):
    s = ''
    for c in string:
        s+=LETTERS[(LETTERS.index(c) + valor) % 26]
    return s
def merge(string, string2):
    s=''
    for i in range(len(string)):
        letra = LETTERS.index(string[i])
        valor = LETTERS.index(string2[i])
        
        s += LETTERS[(letra+valor)%26]
    return s

LETTERS= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

message = leer_sinsalto()

lenM = len(message) //2

mitad1, mitad2 = message[:lenM], message[lenM:]

r1 = rotation_value(mitad1)
r2 = rotation_value(mitad2)

s1 = rotar_string(mitad1, r1)
s2 = rotar_string(mitad2, r2)

print(merge(s1, s2))